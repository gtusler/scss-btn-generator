This button generator attempts to make it easy to create and customize new button styles.


Basic usage looks something like:
```scss
@include new-btn('primary', (
  background: #1873e2,
  background-hover: #00f,
  color: #fff,
  color-hover: #eee,
  border: #1873e2,
  border-hover: #00f
));
```

By default, this will create outlined versions of the buttons and skewed versions of both of them. To disable this functionality, use:
```scss
@include new-btn('primary', $colormap, (outline: false, skew: false));
```

You can't yet pass custom colours to the skewed button (and I'm not sure you should be able to), but you can define custom outline buttons like so:
```scss
@include new-btn(
  'primary',
  (
    background: #1873e2,
    background-hover: #00f,
    color: #fff,
    color-hover: #eee,
    border: #1873e2,
    border-hover: #00f,
    outline-focus: transparentize(#1873e2, .2),
  ),
  (outline: (
    background: transparent,
    background-hover: #1873e2,
    color: #1873e2,
    color-hover: #eee,
    border: #1873e2,
    border-hover: tint(#1873e2, 20%),
  ), skew: true)
);
```

In theory, a new button can be created by writing as little as:
```scss
@include new-btn('primary', (background: #1873e2));
```
and the rest will be figured out for you. In practice, I feel like I haven't tested this.
